#!/bin/sh

python3 setup.py clean --all
rm -rf MANIFEST .coverage dist/ldap0* build/* *.egg-info .tox .eggs docs/.build/* .mypy_cache
rm -f _libldap0.so _libldap0.cpython*.so ldap0/*.py? ldap0/*/*.py? tests/*.py? *.py?
find -name __pycache__ | xargs -iname rm -r name
rm -rf slapdtest-[0-9]*
