# -*- coding: utf-8 -*-
"""
Automatic tests for ldap0
"""

from . import test_about
from . import test_base
from . import test_cache
from . import test_cext
from . import test_cidict
from . import test_controls
from . import test_dn
from . import test_filter
from . import test_functions
from . import test_modlist
from . import test_schema_parse
from . import test_ldapurl
from . import test_ldif
from . import test_ldapobject
from . import test_extensions
from . import test_ppolicy
from . import test_schema_subentry
from . import test_openldap
from . import test_extop
