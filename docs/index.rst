###################
ldap0 Documentation
###################

.. topic:: Abstract

   This document describes the package ldap0 with its various modules.

   Depending on what you want to do this manual assumes basic to expert
   knowledge about the Python language and the LDAP standard (LDAPv3).


********
Contents
********

.. toctree::
   :maxdepth: 3

   installing.rst
   ldap0.rst
   ldap0-controls.rst
   ldap0-dn.rst
   ldap0-extop.rst
   ldap0-filter.rst
   ldap0-modlist.rst
   ldap0-schema.rst
   ldap0-syncrepl.rst
   ldap0-sasl.rst
   ldap0-ldif.rst
   ldap0-ldapurl.rst
   ldap0-test.rst


******************
Indices and tables
******************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
